var app = angular.module('neotech.loginService', [])

app.service("LoginService", function($http, $q, Session){

  this.adminLogin = function(username, password){
    var defer = $q.defer();

    window.data.student_id  = 1;
    window.data.studentName = "Admin";
    window.data.username    = username;
    window.data.password    = username;
    window.data.email       = "admin@gmail.com";
    window.data.address     = "test address";
    window.data.standard    = "XII";
    window.data.firstLogin  = false;
    window.data.api_session_id = "token";

    Session.saveStorage();
    // defer.reject({ success : false });
     defer.resolve({ success : true, data : window.data });
    return defer.promise;
  }


  this.adminLogout = function(){
    var defer = $q.defer();

    window.data.student_id  ="";
    window.data.studentName = "";
    window.data.username    = "";
    window.data.password    = "";
    window.data.email       = "";
    window.data.address     = "";
    window.data.standard    = "";
    window.data.firstLogin  = true;
    window.data.api_session_id = "";

    Session.saveStorage();
     defer.resolve({ success : true, data : window.data });
    return defer.promise;
  }

}); //login service function
