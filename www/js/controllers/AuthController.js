var app = angular.module('neotech.loginController',[]);
app.controller('LoginCtrl', function($scope,$ionicPopup,$state,LoginService) {
	$scope.loginData = {username:"",password:""};
    $scope.doLogin = function() {
    	if($scope.loginData.username.trim().length == 0 || $scope.loginData.password.trim().length == 0){
    		$ionicPopup.show({
    			title: 'Please enter the username & password',
    			buttons: [{
    				text: '<b>OK</b>',
    				type: 'button-balanced'
    			}]
    		});
    	}else{
    		if($scope.loginData.username == "admin" && $scope.loginData.password == "admin")
    		{
	    		LoginService.adminLogin($scope.loginData.username, $scope.loginData.password)
	    		.then(function(response){
	    						$scope.loginData = {username:"",password:""};
								$state.go('app.browse', { reload : true });
							}, function(error){
								$ionicPopup.show({
									title: 'Please enter the correct username & password',
									buttons: [{
										text: '<b>OK</b>',
										type: 'button-balanced'
									}]
								});
							});
    		}else{
    			$ionicPopup.show({
    				title:"Invalid username and password. please try again.",
    				buttons:[{
    					text:"<b>Ok</b>",
    					type: 'button-balanced'
    				}]
    			});
    		}
    	}
};



});